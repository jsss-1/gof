package com.js;

/**
 * Mp4文件的播放器
 */
public class Mp4Player implements AdvanceMediaPlayer{

    @Override
    public void playVlc(String fileName) {
        //不用关注此方法
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("当前正在播放Mp4文件，文件名是"+fileName);
    }
}
