package com.js;

/**
 * 高级媒体播放器的接口
 */
public interface AdvanceMediaPlayer {
    void playVlc(String fileName);
    void playMp4(String fileName);
}
