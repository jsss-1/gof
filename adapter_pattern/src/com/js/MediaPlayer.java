package com.js;

/**
 * 媒体播放器的接口
 */
public interface MediaPlayer {
    //播放音频文件的方法
    void play(String audioType,String fileName);

}
