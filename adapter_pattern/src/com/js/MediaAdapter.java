package com.js;

/**
 * 媒体适配器类
 */
public class MediaAdapter implements MediaPlayer{
    //适配器依赖于高级媒体播放器的接口
    private AdvanceMediaPlayer advanceMediaPlayer;

    //对当前的高级播放器做初始化操作
    public MediaAdapter(String audioType) {
        if (audioType.equalsIgnoreCase("vlc")){//播放vlc类型的文件
            advanceMediaPlayer = new VlcPlayer();//多态：向上转型
        }else if (audioType.equalsIgnoreCase("mp4")){
            advanceMediaPlayer = new Mp4Player();
        }
    }

    @Override
    public void play(String audioType, String fileName) {
        //根据当前的文件类型来选择调用哪一个播放器
        if (audioType.equalsIgnoreCase("vlc")){//播放vlc类型的文件
            advanceMediaPlayer.playVlc(fileName);
        }else if (audioType.equalsIgnoreCase("mp4")){
            advanceMediaPlayer.playMp4(fileName);
        }
    }
}
