package com.js;

/**
 * Vlc文件的播放器
 */
public class VlcPlayer implements AdvanceMediaPlayer{

    @Override
    public void playVlc(String fileName) {
        System.out.println("当前正在播放Vlc文件，文件名是"+fileName);
    }

    @Override
    public void playMp4(String fileName) {
        //不用关注
    }
}
