package com.js;

public class AdapterPattern {
    public static void main(String[] args) {
        //只能播放mp3类型的对象
        AudioPlayer audioPlayer=new AudioPlayer();
        audioPlayer.play("mp3","后来");
        audioPlayer.play("vlc","故乡");
        audioPlayer.play("mp4","蝙蝠侠");
        audioPlayer.play("avi","遇见");

    }
}
