package com.js;

/**
 * 音频文件播放器类:可以播放mp3文件，如果传递的是mp4和vlc需要调用适配器来进行播放
 */
public class AudioPlayer implements MediaPlayer{
    //这个对象需要进行初始化操作
    private MediaAdapter mediaAdapter;//高级媒体播放的适配器
    @Override
    public void play(String audioType, String fileName) {
        if (audioType.equalsIgnoreCase("mp3")){
            System.out.println("正在播放mp3文件,文件名称是"+fileName);
        }else if (audioType.equalsIgnoreCase("vlc")||
                audioType.equalsIgnoreCase("mp4")){//调用高级适配器来完成播放
            //创建当前的适配器对应的对象
            mediaAdapter=new MediaAdapter(audioType);
            mediaAdapter.play(audioType,fileName);//桥梁搭建成功
        }else {
            System.out.println("您要播放的文件格式不支持");
        }
    }
}
