package com.js;

/**
 * 食物条目的接口
 */
public interface Item {
    String name();
    Packing packing();
    float price();
}
