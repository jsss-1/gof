package com.js;

/**
 * 汉堡的抽象类
 */
public abstract class Burger implements Item{

    @Override
    public Packing packing() {
        return new Wrapper();//纸盒包装
    }

}
