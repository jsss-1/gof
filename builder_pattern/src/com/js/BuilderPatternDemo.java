package com.js;

/**
 * 客户类
 */
public class BuilderPatternDemo {
    public static void main(String[] args) {
        MealBuilder mealBuilder=new MealBuilder();
        Meal veg = mealBuilder.prepareNonVegMeal();
        veg.showItems();
    }
}
