package com.js;

/**
 * 食物条目的接口 （汉堡打包和冷饮打包方式）
 */
public interface Packing {
    String pack();//食物打包方法
}
