package com.js;

import java.util.ArrayList;
import java.util.List;

/**
 * 进餐类：维护用户点的事务条目
 */
public class Meal {
    private List<Item> items = new ArrayList<>();

    //将用户点的事务条目添加到集合中
    public void addItem(Item item){
        items.add(item);
    }

    //获取所有点餐事务的总价格
    public float getCost(){
        float cost=0;
        for (Item item : items) {
            cost+=item.price();
        }
        return cost;
    }

    //获取用户点餐明细
    public void showItems(){
        for (Item item : items) {
            System.out.println("名字："+item.name());
            System.out.println("包装："+item.packing());
            System.out.println("价格："+item.price());
            System.out.println("----------------------");
        }
    }
}
