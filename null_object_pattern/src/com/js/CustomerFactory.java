package com.js;

public class CustomerFactory {
    private  static final String[] names={"tom","jack","coco"};

    //定义一个方法，根据用户传递的名称来判断当前是否存在此客户
    //如果不存在，则抛出指定空对象（NullCustomer）
    public static AbstractCustomer getCustomer(String name){
        for (int i = 0; i < names.length; i++) {
            //如果if成立，则表示目标访问的对象存在（RealCustomer）
            if (names[i].equalsIgnoreCase(name)){
                return new RealCustomer(name);
            }
        }
        return new NullCustomer();//彻底避免Null对象的产生
    }
}
