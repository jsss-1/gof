package com.js;

/**
 * 表示目标要操作的类
 */
public class RealCustomer extends AbstractCustomer{

    public RealCustomer(String name) {
        this.name=name;
    }

    @Override
    public boolean isNil() {
        return false;
    }

    @Override
    public String getName() {
        return name;//直接访问父类中的name
    }
}
