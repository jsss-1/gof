package com.js;

/**
 * 定义一个抽象类
 */
public abstract class AbstractCustomer {
    public String name;//null

    //是否是空值
    public abstract boolean isNil();

    //获取名称
    public abstract String getName();

}
