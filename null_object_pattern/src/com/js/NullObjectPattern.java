package com.js;

public class NullObjectPattern {
    public static void main(String[] args) {
        AbstractCustomer c1 = CustomerFactory.getCustomer("tom");
        System.out.println(c1.getName());

        AbstractCustomer c2 = CustomerFactory.getCustomer("marry");
        System.out.println(c2.getName());

    }
}
