package com.js;

public class NullCustomer extends AbstractCustomer{
    @Override
    public boolean isNil() {
        return true;//是空值
    }

    @Override
    public String getName() {
        return "没有定义当前的对象";
    }
}
