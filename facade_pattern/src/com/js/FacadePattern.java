package com.js;

/**
 * 客户端看待（尽量简单，完成功能）
 */
public class FacadePattern {
    public static void main(String[] args) {
        ShapeMaker maker=new ShapeMaker();
        maker.drawCircle();
        maker.drawRectangle();
        maker.drawSquare();
    }
}
