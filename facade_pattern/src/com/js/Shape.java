package com.js;

/**
 * 绘制图形的接口
 */
public interface Shape {
    void draw();
}
