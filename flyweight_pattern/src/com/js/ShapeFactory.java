package com.js;

import java.util.HashMap;

public class ShapeFactory {
    /**
     * key：表示颜色的值
     * value:Shape接口类型的对象
     */
    private static HashMap<String ,Shape> circleMap=new HashMap<>();

    //获取圆形对象，依据颜色值来获取
    public static Shape getCircle(String color){
        //去Map集合中找，如果集合中有则直接通过集合返回，不用new
        Circle circle= (Circle) circleMap.get(color);
        if (circle==null){
            //创建对象
            circle=new Circle(color);
            //共享到Map集合中
            circleMap.put(color,circle);
            System.out.println("创建圆形["+color+"]颜色成功");
        }
        return circle;
    }
}
