package com.js;

public class FlyWeightPattern {
    static String[] colors={"red","blue","yellow","white","black"};
    public static void main(String[] args) {

        //生成20个圆形
        for (int i = 0; i < 20; i++) {
            Circle circle = (Circle) ShapeFactory.getCircle(getRandomColor());
            circle.setX((int) (Math.random()*100));
            circle.setY((int) (Math.random()*100));
            circle.setRadius((int) (Math.random()*20));
            //调用对象的绘制方法来绘制圆形
            circle.draw();
        }
    }
    public static String getRandomColor(){
        //0~4之间
        return colors[(int)(Math.random()* colors.length)];
    }
}
