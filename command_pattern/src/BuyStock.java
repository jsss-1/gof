/**
 * 购买对应的股票
 */
public class BuyStock implements Order{
    //依赖于Stock对象
    private Stock stock;

    //构造方法
    public BuyStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public void execute() {//由此方法完成
        //购买目标股票的业务操作
        stock.buy();
    }
}
