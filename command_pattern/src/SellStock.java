/**
 * 股票出售类
 */
public class SellStock implements Order{
    private Stock stock;

    public SellStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public void execute() {
        stock.sell();//完成股票的出售操作
    }
}
