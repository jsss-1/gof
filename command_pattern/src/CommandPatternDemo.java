public class CommandPatternDemo {
    public static void main(String[] args) {
        Stock stock = new Stock("002607", 100);
        BuyStock buyStock = new BuyStock(stock);
        Broker broker=new Broker();
        broker.takeOrder(buyStock);

        //执行订单的交易动作
        broker.placeOrders();

    }
}
