import java.util.ArrayList;
import java.util.List;

/**
 * 股票经纪人类
 */
public class Broker {
    private List<Order> orderLists=new ArrayList<>();
    //1.接受订单
    public void takeOrder(Order order){
        orderLists.add(order);
    }

    //2.下订单（购买、出售）
    public void placeOrders(){
        for (Order order:orderLists) {
            order.execute();//执行订单操作（下订单或者出售订单）
        }
    }
}
