/**
 * 股票类
 */
public class Stock {
    //股票代码
    private String name;
    //股票数量
    private int quality;

    public Stock(String name, int quality) {
        this.name = name;
        this.quality = quality;
    }


    public void sell(){
        System.out.println("股票出售："+name+",股数："+quality);
    }

    public void buy(){
        System.out.println("股票购买："+name+",股数："+quality);
    }

}
