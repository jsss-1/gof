package com.js;

/**
 * 板球运动
 */
public class Cricket extends Game{
    @Override
    public void initialize() {
        System.out.println("板球游戏开始初始化...");
    }

    @Override
    public void startPlay() {
        System.out.println("板球游戏正在执行中...");
    }

    @Override
    public void endPlay() {
        System.out.println("板球游戏结束！");
    }
}
