package com.js;

/**
 * 表示游戏的模板类（方法有具体的子类完成）
 * 定义成抽象类
 */
public abstract class Game {
    //游戏初始化
    public abstract void initialize();
    //开始游戏
    public abstract void startPlay();
    //结束游戏
    public abstract void endPlay();

    //定义游戏操作方法（模板方法）,当前的方法不被子类重写
    public final void play(){
        initialize();
        startPlay();
        endPlay();
    }

}
