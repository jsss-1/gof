package com.js;

/**
 * 足球运动
 */
//当前类如果还想实现其他的父类，扩展将会违背单继承
public class FootBall extends Game{
    @Override
    public void initialize() {
        System.out.println("足球游戏初始化...");
    }

    @Override
    public void startPlay() {
        System.out.println("足球游戏正在执行中...");
    }

    @Override
    public void endPlay() {
        System.out.println("足球游戏结束！");
    }
}
