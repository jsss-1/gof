package com.js;

/**
 * 正方形类
 */
public class Square extends Shape{
    public Square(){
        type="正方形";
    }

    @Override
    void draw() {
        System.out.println("绘制一个正方形");
    }
}
