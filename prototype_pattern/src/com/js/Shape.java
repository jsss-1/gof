package com.js;

/**
 * 图形类
 *
 * Cloneable接口：
 * 1.功能：表示可以通过克隆的技术，实现对象的快速创建
 * 2.实现：直接对类的实现Cloneable接口
 * 3.方法：编写一个方法clone()方法。用来表示具体的克隆。
 */
public abstract class Shape implements Cloneable{
    private String id;
    //类型需要在子类中确定（需要在子类中访问父类中的成员）
    protected String type;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    /*
    public void setType(String type) {
        this.type = type;
    }
     */

    abstract void draw();

    public Object clone()  {
        Object clone=null;
        try {
            //可以依赖当前类的模板自动克隆一个一模一样的对象
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
