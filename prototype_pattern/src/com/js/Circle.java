package com.js;

/**
 * 圆形类
 */
public class Circle extends Shape{
    public Circle(){
        type="圆形";
    }

    @Override
    void draw() {
        System.out.println("绘制一个圆形");
    }
}
