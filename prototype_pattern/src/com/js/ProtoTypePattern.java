package com.js;

public class ProtoTypePattern {
    public static void main(String[] args) {
        //加载初始的原始对象
        ShapeCache.load();

        Shape cloneCircle = ShapeCache.getShape("1");
        System.out.println(cloneCircle.getType());

        Shape cloneRectangle = ShapeCache.getShape("3");
        System.out.println(cloneRectangle.getType());
    }
}
