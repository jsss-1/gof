package com.js;

import java.util.Hashtable;

public class ShapeCache {
    //创建一个对应的Hashtable的存储
    private static Hashtable<String,Shape> shapeMap=new Hashtable<>();

    //模拟在当前的存储Map中存放三个原始的对象
    public static void load(){
        Circle circle=new Circle();
        circle.setId("1");
        shapeMap.put(circle.getId(), circle);

        Square square=new Square();
        square.setId("2");
        shapeMap.put(square.getId(), square);

        Rectangle rectangle=new Rectangle();
        rectangle.setId("3");
        shapeMap.put(rectangle.getId(), rectangle);

    }

    //调用根据id来指定需要获取的对象（1-圆形、2-矩形...）
    //克隆对象，需要调用对象的clone方法来完成（向上转型）
    public static Shape getShape(String shapeId){
        Shape shape = shapeMap.get(shapeId);//根据用户传递的id值来获取对象
        return (Shape) shape.clone();
    }
}
