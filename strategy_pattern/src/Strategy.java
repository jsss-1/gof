/**
 * 策略接口
 */
public interface Strategy {
    int doOperation(int num1,int num2);//算数运算操作方法
}
