/**
 * Context表示使用哪种策略对应的类
 */
public class Context {
    //依赖于策略的接口（不能依赖实现类）
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    //执行策略业务逻辑（来接受对应的策略执行结果）
    public int executeStrategy(int num1,int num2){
        return strategy.doOperation(num1,num2);//调用真正的策略执行者
    }
}
