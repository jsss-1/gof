package com.jsss;

/**
 * 登记式/静态内部类：
 *  1.在单例类中什么一个静态内部类
 *  2.在这个静态内部类中声明单例对象，并且初始化这个对象
 */
public class RegisterSingleton {
    //1.在单例类中什么一个静态内部类
    //2.在这个静态内部类中声明单例对象，并且初始化这个对象
    private static class SingletonHolder{
        private static final RegisterSingleton INSTANCE = new RegisterSingleton();
    }

    public static final RegisterSingleton getInstance(){
        return SingletonHolder.INSTANCE;
    }

    private RegisterSingleton(){}

    public void showMessage(){
        System.out.println("RegisterSingleton的showMessage()方法执行");
    }
}
