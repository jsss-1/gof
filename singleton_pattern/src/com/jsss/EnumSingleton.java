package com.jsss;

/**
 * 枚举式
 */
public enum EnumSingleton {
    //枚举常量可以作为枚举内部定义的方法的访问
    //INSTANCE.showMessage()
    INSTANCE;
    /**
     * 1.枚举类型中定义的变量只能是常量
     * 2.枚举类中定义方法（和class类中结构中定义的方法完全一样）
     */
    public void showMessage(){
        System.out.println("EnumSingleton枚举类中showMessage");
    }
}
