package com.jsss;

/**
 * 饿汉式：提前将对象准备好，加载到内存中
 */
public class HungrySingleton {
    //加载不依赖 与类的new方式操作，可以使用static修饰
    private static HungrySingleton instance = new HungrySingleton();

    //将当前类的构造方法私有化处理
    private HungrySingleton(){}

    //提供一个公共的访问方法来访问类中的私有成员,什么成静态方法
    public static HungrySingleton getInstance(){
        return instance;
    }

    //编写业务逻辑
    public void showMessage(){
        System.out.println("HungrySingleton的showMessage()方法执行");
    }
}
