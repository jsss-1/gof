package com.jsss;

/**
 * 懒汉式：什么时候使用对象什么时候创建对象
 */
public class LazySingleton {
    //什么对象的时候没有初始化
    private static LazySingleton instance;

    private LazySingleton(){

    }

    public static synchronized LazySingleton getInstance(){
        if (instance == null){
            instance = new LazySingleton();
        }
        return instance;
    }

    public void showMessage(){
        System.out.println("LazySingleton的showMessage()方法执行");
    }

}
