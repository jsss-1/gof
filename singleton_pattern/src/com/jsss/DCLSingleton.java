package com.jsss;

/**
 * 双重校验锁
 */
public class DCLSingleton {
    /**
     * volatile；表示特征修饰符（type specifier），作用是一个指令关键，
     * 确保本条指令所修饰的成员不会在编译的时候被省略，且每次会直接读取它的值。
     * 防止编译器优化此代码的风险。
     */
    private volatile static DCLSingleton instance;

    private DCLSingleton(){}

    public static  DCLSingleton getInstance(){
        if (instance == null){
            synchronized(DCLSingleton.class){
                if (instance == null){
                    instance = new DCLSingleton();
                }
            }
        }
        return instance;
    }

    public void showMessage(){
        System.out.println("DCLSingleton的showMessage()方法执行");
    }
}
