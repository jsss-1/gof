package com.jsss;

public class SingletonPatternDemo {
    public static void main(String[] args) {
        //1.饿汉式-获取对象
        HungrySingleton HungrySingleton1 = HungrySingleton.getInstance();
        HungrySingleton1.showMessage();

        //2.懒汉式-获取对象
        LazySingleton lazySingleton = LazySingleton.getInstance();
        lazySingleton.showMessage();

        //3.双重校验锁-获取对象
        DCLSingleton dclSingleton = DCLSingleton.getInstance();
        dclSingleton.showMessage();

        //4.静态内部类-获取对象
        RegisterSingleton registerSingleton = RegisterSingleton.getInstance();
        registerSingleton.showMessage();

        //5.枚举方式-获取对象
        EnumSingleton.INSTANCE.showMessage();
    }
}
