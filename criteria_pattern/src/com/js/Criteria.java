package com.js;

import java.util.List;

/**
 * 过滤器的接口（标准接口）
 */
public interface Criteria {
    /**
     * 实现的符号指定的过滤条件的方法，需要将对应的Person对象
     * 以参数的形式进行传递
     */
    /**
     * 方法表示一个过滤的方法
     * @param persons 被过滤的对象
     * @return 满足条件的对象的集合
     */
    List<Person> meetCriteria(List<Person> persons);
}
