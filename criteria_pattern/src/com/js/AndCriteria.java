package com.js;

import java.util.ArrayList;
import java.util.List;


public class AndCriteria implements Criteria{
    private Criteria criteria;
    private Criteria otherCriteria;

    public AndCriteria(Criteria criteria, Criteria otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> firstPersons =new ArrayList<>();
        List<Person> secondPersons =new ArrayList<>();

        firstPersons = criteria.meetCriteria(persons);
        secondPersons = otherCriteria.meetCriteria(firstPersons);
        return secondPersons;
    }
}
