package com.js;

import java.util.ArrayList;
import java.util.List;

/**
 * 女性过滤类
 */
public class CriteriaFemale implements Criteria{
    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        /**
         * male、female
         */
        List<Person> femalePersons =new ArrayList<>();
        for (Person person : persons){
            if (person.getGender().equalsIgnoreCase("female")){
                femalePersons.add(person);
            }
        }
        return femalePersons;
    }
}
