package com.js;

import java.util.ArrayList;
import java.util.List;

/**
 * 男性过滤类
 */
public class CriteriaMale implements Criteria{
    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        /**
         * male、female
         */
        List<Person> malePersons=new ArrayList<>();
        for (Person person : persons){
            if (person.getGender().equalsIgnoreCase("male")){
                malePersons.add(person);
            }
        }
        return malePersons;
    }
}
