package com.js;

import java.util.ArrayList;
import java.util.List;

public class FilterPatternDemo {
    public static void main(String[] args) {
        //构建一个List集合，然后在这个集合中保存不同person信息的对象
        List<Person> peoples=new ArrayList<>();
        Person p1= new Person("张三","male","single");
        Person p2= new Person("李四","female","marital");
        Person p3= new Person("王五","female","single");
        Person p4= new Person("赵六","male","marital");
        Person p5= new Person("张三","male","single");
        Person p6= new Person("张三","male","single");
        Person p7= new Person("张三","male","marital");
        peoples.add(p1);
        peoples.add(p2);
        peoples.add(p3);
        peoples.add(p4);
        peoples.add(p5);
        peoples.add(p6);
        peoples.add(p7);

        //创建一个过滤器对象（male筛选出来）
        CriteriaMale male=new CriteriaMale();
        List<Person> males = male.meetCriteria(peoples);
        System.out.println(males);

        System.out.println("--------------------");

        CriteriaSingle single=new CriteriaSingle();
        List<Person> singles = single.meetCriteria(peoples);
        System.out.println(singles);

        System.out.println("--------------------");

        AndCriteria and=new AndCriteria(male,single);
        List<Person> ands = and.meetCriteria(peoples);
        System.out.println(ands);

    }
}
