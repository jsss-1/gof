package com.js;

import java.util.ArrayList;
import java.util.List;


public class OrCriteria implements Criteria{
    private Criteria criteria;
    private Criteria otherCriteria;

    public OrCriteria(Criteria criteria, Criteria otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> firstPersons =new ArrayList<>();
        List<Person> secondPersons =new ArrayList<>();

        firstPersons = criteria.meetCriteria(persons);
        secondPersons = otherCriteria.meetCriteria(persons);

        for (Person person : secondPersons){
            if (!firstPersons.contains(person)){
                firstPersons.add(person);
            }
        }
        return firstPersons;

    }
}
