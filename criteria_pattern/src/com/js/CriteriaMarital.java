package com.js;

import java.util.ArrayList;
import java.util.List;


public class CriteriaMarital implements Criteria{
    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> maritalPersons =new ArrayList<>();
        for (Person person : persons){
            if (person.getMaritalStatus().equalsIgnoreCase("marital")){
                maritalPersons.add(person);
            }
        }
        return maritalPersons;
    }
}
