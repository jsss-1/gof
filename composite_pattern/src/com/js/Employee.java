package com.js;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    //姓名
    private String name;
    //部门
    private String dept;
    //薪水
    private int salary;
    //List集合存储当前员工的下属
    private List<Employee> subordinate;

    //构造方法完成员工信息的初始化操作
    public Employee(String name, String dept, int salary) {
        this.name = name;
        this.dept = dept;
        this.salary = salary;
        //重构：用来维护当前员工是否有下属的一个属性
        this.subordinate = new ArrayList<>();
    }

    public void add(Employee employee){
        subordinate.add(employee);
    }

    public void remove(Employee employee){
        subordinate.remove(employee);
    }

    public List<Employee> getSubordinate(){
        return subordinate;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                ", salary=" + salary +
                ", subordinate=" + subordinate +
                '}';
    }
}
