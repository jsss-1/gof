package com.js;

public class CompositePattern {
    public static void main(String[] args) {
        //CEO
        Employee employee=new Employee("张三","CEO",3000000);
        //法定代表人
        Employee emp1=new Employee("李四","法务部",10000);
        //市场总监
        Employee emp2=new Employee("王五","市场总监",20000);
        employee.add(emp1);
        employee.add(emp2);

        Employee emp3=new Employee("小明","市场部",5000);
        Employee emp4=new Employee("小红","市场部",5000);
        emp2.add(emp3);
        emp2.add(emp4);

        //输出当前的员工的树形结构
        System.out.println(employee);

        for (Employee e:employee.getSubordinate()) {
            System.out.println(e);
            for (Employee emp:e.getSubordinate()) {
                System.out.println(emp);
            }
        }
    }
}
