package com.js;

public class BridgePattern {
    public static void main(String[] args) {
        //创建对应的图形，需要传递对应的参数信息
        //DrawApi创建一个符合当前接口规范的对象
        DrawApi drawApi1=new RedCircle();
        Circle circle=new Circle(drawApi1,3,100,200);
        circle.draw();

        DrawApi drawApi2=new GreenCircle();
        Circle circle2=new Circle(drawApi2,3,100,200);
        circle2.draw();
    }
}
