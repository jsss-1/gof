package com.js;

public class Circle extends Shape{

    private  int radius;
    private int x;
    private int y;

    //Alt+Insert
    public Circle(DrawApi drawApi, int radius, int x, int y) {
        super(drawApi);
        this.radius = radius;
        this.x = x;
        this.y = y;
    }

    @Override
    public void draw() {
        //用来表示具体绘制的动作
        //重点：调用具体的实现，来完成图形的绘制
        drawApi.drawCircle(radius,x,y);
    }
}
