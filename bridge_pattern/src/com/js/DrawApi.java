package com.js;

/**
 * 桥接接口
 */
public interface DrawApi {
    void drawCircle(int radius,int x,int y);
}
