package com.js;

public abstract class Shape {
    //重点：抽象类中依赖了具体的实现，具体的实现剥离出去进行了独立的定义
    //抽象与具体解耦，桥接模式关键步骤
    public DrawApi drawApi;//表示颜色填充的属性

    public Shape(DrawApi drawApi){
        this.drawApi=drawApi;
    }

    public abstract void draw();//具体绘制动作实现交给子类来完成

}
