package com.js;

/**
 * 填充绿色的业务类
 */
public class GreenCircle implements DrawApi{
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("填充绿色,"+"radius="+radius+",x="+x+",y="+y);
    }
}
