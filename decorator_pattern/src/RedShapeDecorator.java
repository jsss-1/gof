public class RedShapeDecorator extends ShapeDecorator{
    //调用具体的装饰器类来初始化图形对象
    public RedShapeDecorator(Shape shape) {
        super(shape);
    }

    @Override
    public void draw() {
        //1.目标业务逻辑
        super.draw();
        //2.扩展新功能：就是装饰器的核心代码
        setRedColor();
    }

    private void setRedColor(){
        System.out.println("图形被填充成红色！");
    }
}
