/**
 * 绘制图形接口
 */
public interface Shape {
    void draw();
}
