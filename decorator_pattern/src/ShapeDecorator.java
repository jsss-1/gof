/**
 * 装饰器的规范，具体装饰的实现，可以让具体的子类装饰器来实现具体的装饰效果
 */
public abstract class ShapeDecorator implements Shape{
    //目标装饰的对象
    public Shape shape;

    public ShapeDecorator(Shape shape) {
        this.shape = shape;
    }

    //1.目标方法的调用shape.draw()
    //2.需要扩展新功能（起到了装饰效果）

    @Override
    public void draw() {
        //1.目标方法的调用shape.draw()
        shape.draw();
        //2.需要扩展新功能（起到了装饰效果）
        //System.out.println("当前图形填充成红色");
    }
}
