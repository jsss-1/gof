package com.js;

/**
 * 代理图片显示的类
 */
public class ProxyImage implements Image{
    private  String fileName;//表示图片路径
    private RealImage realImage;//表示真正加载图片的类
    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display() {
        //1.借助真正加载图片的类（RealImage）来完成图片的加载操作
        if (realImage==null){
            //2.初始化加载图片的类对象
            realImage =new RealImage(fileName);
        }
        //3.直接调用当前realImage对象的display()进行图片的展示
        realImage.display();
    }
}
