package com.js;

/**
 * 真正加载图片和显示图片的执行类
 */
public class RealImage implements Image{
    private String fileName;//表示图片的文件路径

    public RealImage(String fileName) {
        this.fileName = fileName;
        //定义一个加载磁盘上图片方法
        loadFormDisk(fileName);
    }

    private void loadFormDisk(String fileName){
        System.out.println("当前正在加载："+fileName);
    }

    @Override
    public void display() {
        System.out.println("显示当前的图片"+fileName);
    }


}
