﻿@[TOC](导航【Java设计模式】)
# 前言

----
2022/9/20 16:00

-----
路漫漫其修远兮,吾将上下而求索

---
**本文是根据袁庭新老师学习所做笔记**

**仅供学习交流使用，转载注明出处**

---
# 推荐

袁庭新老师-视频：[【Java设计模式】23种Java设计模式，全网最全精讲版本，熬夜爆肝也要学完](https://www.bilibili.com/video/BV1Mg411U7D6)【10:06:48】

狂神老师-笔记：[设计模式【java提高】](https://blog.csdn.net/qq_51625007/article/details/125872560)

[git 代码](https://gitee.com/jsss-1/gof) 

# 导航【Java设计模式】
[导航【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127042855)

[设计模式简介【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/126955946)

[单例模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/126957592)

[建造者模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/126960880)

[原型模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/126991143)

[适配器模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127008485)

[桥接模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127008962)

[过滤器模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127009607)

[装饰器模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127027542)

[组合模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127030896)

[外观模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127031280)

[享元模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127031724)

[代理模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127038458)

[策略模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127038911)

[模板模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127039334)

[空对象模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127041278)

[命令模式【Java设计模式】](https://blog.csdn.net/qq_51625007/article/details/127042304)

# 另外


![在这里插入图片描述](https://img-blog.csdnimg.cn/8df5335e02a148f29f386856baca64b3.png)


# 最后

---
2022/9/25 21:00

----
p1~p16

---

经过五天学习，也是学习完了袁庭新老师的设计模式。

袁老师还是讲的挺不错的。

仅以此篇，致敬！


---
